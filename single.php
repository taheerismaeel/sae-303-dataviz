<?php get_header(); ?>

<main>
    <h1>Single</h1>
</main>

<div class="navigation">
    <div class="previous-post-link"><?php previous_post_link('%link', 'Article précédent : %title'); ?></div>
    <div class="next-post-link"><?php next_post_link('%link', 'Article suivant : %title'); ?></div>
</div>

<?php get_footer(); ?>