<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta charset="<?php echo bloginfo ('charset');?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/style/accueil.css">
  


<?php get_header(); ?>

<body>

<?php if( have_posts() ) : while( have_posts() ) : the_post(); ?>
    
        <!-- <h1><?php the_title(); ?></h1>
    
        <?php the_content(); ?> -->

    <?php endwhile; endif; ?>



<main>
    <!-- <div class="menu-container">
        <?php wp_nav_menu( array( 'menu' => 'navbar1' ) ); ?>
        <?php wp_nav_menu( array( 'menu' => 'navbar2' ) ); ?>
    </div> -->

    <div class="grid-container">
        <div class="grid-item">
        <img src="<?= get_template_directory_uri(); ?>/media/image.png" class="image" alt="image">
    </div>
  <div class="grid-item">
    <p class="text">Ce site web a été conçu dans le but d'illustrer les différentes visualisations de données que nous avons étudiées au cours de la session d'apprentissage expérientiel précédente. Il constitue un outil visuel essentiel pour mettre en pratique les concepts et les techniques abordés lors de cette phase de formation.

Chaque section du site web est dédiée à une catégorie spécifique de datavisualisations, permettant ainsi aux utilisateurs de naviguer facilement à travers les différents types de graphiques et de représentations de données. De cette manière, les visiteurs peuvent explorer et analyser diverses formes de visualisation de données.</p>
  </div>


</div>

        <h3 id="2">Popularité des morceaux en fonction du bpm</h3>
        <div id="dataviz1"></div>
        <p id="para">L’axe des x représente l’énergie en pourcentage, allant de 0 à 100%.
        L’axe des y  représente le nombre d’écoutes, avec des valeurs allant de 0 à environ 3,5 milliards.
        Le graphique est un nuage de points avec chaque point représentant une observation particulière.
        La majorité des points sont concentrés dans la partie inférieure du graphique, indiquant un nombre d’écoutes plus élevé pour les niveaux d’énergie plus bas. Quelques points sont dispersés à travers le milieu et le haut du graphique, indiquant que certaines chansons avec des niveaux d’énergie plus élevés ont également été écoutées, mais pas autant que celles avec des niveaux d’énergie plus bas.
        Il n’y a pas de tendance claire ou de corrélation visible entre l’énergie et le nombre d’écoutes dans ce graphique. Cela pourrait suggérer que le niveau d’énergie d’une chanson n’est pas un facteur déterminant du nombre d’écoutes qu’elle reçoit. 
        </p>

     

            <h3 id="3">Dançabilité en fonction du nombre d'écoute</h3>
        <div class="essai">
            <div id="dataviz2"></div>
            <p id="para">Ce graphique représente la dançabilité des chansons en fonction de leur nombre d'écoutes. Chaque point sur le graphique représente une chanson unique.
            <br><br>
            L'axe des abcisses (horizontal) représente la dançabilité des chansons. La dançabilité décrit à quel point une chanson est adaptée à la danse. <br>
            L'axe des ordonnées (vertical) représente le nombre d'écoutes de chaque chanson. <br><br>
            En passant la souris sur un point, vous pouvez voir la dançabilité, le nombre d'écoutes, ainsi que le nom de la chanson et de l'artiste.
            <br>
            Ce graphique peut nous aider à comprendre si les chansons plus dansantes ont tendance à être plus écoutées et inversement.
            </p>

        </div>

        <h3 id="4">Clé des musiques les plus écouté</h3>
        <div id="dataviz4"></div>
        <p id="para">Ce graphique à barres horizontales représente la popularité des différentes clés de musique en fonction du nombre d’écoutes. Chaque barre sur le graphique représente une clé de musique unique.
        <br><br>
        L’axe des abcisses (horizontal) représente les différentes clés de musique, allant de A à G#. La clé de musique décrit la tonalité d’une chanson.
        <br>
        L’axe des ordonnées (vertical) représente le nombre d’écoutes pour chaque clé de musique.
        <br><br>
        On constate que la clé C# est la plus écoutée, ce qui indique qu’elle est particulièrement populaire parmi les auditeurs.
        Ce graphique peut nous aider à comprendre si certaines clés de musique ont tendance à être plus écoutées que d’autres.
        <br><br><br>
        PS : La 1ère barre concerne les musiques dont la clé n'a pas été renseigné. 
        En résumé, ce graphique permet de visualiser la popularité des chansons sur Spotify, à la fois en termes de nombre d’écoutes et de fréquence d’ajout dans les playlists.
        </p>

</main>
    
</body>



<?php get_footer(); ?>