<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta charset="<?php echo bloginfo ('charset');?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/style/style.css">

    <?php wp_head();?>
</head>
<body <?php body_class();?>>
    <?php wp_body_open();?>

    <?php
    if (is_user_logged_in()) {
        echo "<div>Message pour les utilisateurs connectés à l'administration</div>";
    }
    ?>

    <header class="header-container">
        <a href="<?php echo home_url();?>">
            <img src="<?= get_template_directory_uri(); ?>/media/logoSpotify.png" class="logo" alt="logo">
        </a>

        <nav>
        <ul>
            <li><a href="#2">Popularité</a></li>
            <li><a href="#3">Dançabilité</a></li>
            <li><a href="#4">Clé</a></li>
        </ul>
    </nav>
    </header>
</body>
</html>
