//dataviz 1
function unpack(rows, key) {
    return (rows.map((row) => {
        return row[key];
    }))
}
function draw(div, rows) {
    const bpm = unpack(rows, 'bpm');
    const streams = unpack(rows, 'streams');
    const noms = unpack(rows, 'track_name');
    const artistes = unpack(rows, 'artist(s)_name');

    let streams2=[];
    streams2 = streams.filter( (elt) => {
        return elt > 10000000
    });

    const trace = {
        x: bpm,
        y: streams2,
        mode: 'markers',
        marker: { color: '#1ed760'},
        type: 'scatter',
        text: noms.map((nom, i) => `${nom} par ${artistes[i]}`),
        hovertemplate: '%{x} bpm, %{y} d\'écoutes<br>%{text}',
        name: '',
    }
    const layout = {
        xaxis: {
            title: 'BPM'
        },
        yaxis: {
            type: 'log',
            title: 'Streams',             
        },
        title: 'Popularité des morceaux en fonction du bpm'
    };
    console.log(bpm);
    Plotly.newPlot('dataviz1', [trace], layout);
}
async function main() {

    const rows = await d3.csv("./wp-content/themes/themesperso1/dataset/streamed.csv");
    const myDiv = document.getElementById('dataviz1');
    draw(myDiv, rows);
}
main();
//dataviz 1


//dataviz 2
function unpack2(rows, key) { 
    return (rows.map((row) => { 
        return row[key];    
    }))
}
function draw2(div, rows) { 
    const dançabilité = unpack2(rows, 'danceability_%'); 
    const écoutes = unpack2(rows, 'streams');
    const noms = unpack2(rows, 'track_name');
    const artistes = unpack2(rows, 'artist(s)_name');

    const trace = {
        x: dançabilité,
        y: écoutes,
        mode: 'markers',
        type: 'scatter',
        text: noms.map((nom, i) => `${nom} par ${artistes[i]}`),
        hovertemplate: '%{x}% dançabilité, %{y} d\'écoutes<br>%{text}',
        marker: { color: '#1ed760' },
        name: ''
    };

    const layout = {
        width: 800,
        height: 500, 
        title: 'Dançabilité en fonction du nombre d\'écoute',
        xaxis: {
            title: 'Dançabilité (%)'
        },
        yaxis:{
            title:'Nombre d\'Écoutes'
         }
    };

    Plotly.newPlot('dataviz2', [trace], layout);
}
async function main2() {
    const rows = await d3.csv("./wp-content/themes/themesperso1/dataset/streamed.csv");
    console.log(rows);
    const plot = document.getElementById('dataviz2');
    draw2(plot, rows);
}
main2();
//dataviz 2

//dataviz 3
function unpack3(rows, key) {
    return (rows.map((row) => {
        return row[key];
    }))
}
function draw3(div, rows) {
    const fav_music_genre = unpack3(rows, 'fav_music_genre');

    const trace = {
        labels: fav_music_genre,
        type: 'pie',
    }

    const layout = {
        title: 'Genre musicaux les plus appréciés'
    };

    console.log(fav_music_genre);
    Plotly.newPlot('dataviz3', [trace], layout);
}
async function main3() {

    const rows = await d3.csv("./wp-content/themes/themesperso1/dataset/streamed.csv");
    console.log(rows);
    const myDiv2 = document.getElementById('dataviz3');
    draw3(myDiv2, rows);
}
main3()
//dataviz 3


//dataviz 4
// Charger le dataset
d3.csv("./wp-content/themes/themesperso1/dataset/streamed.csv").then(data => {
    data.sort((a, b) => a.key.localeCompare(b.key)); //pour afficher les clés dans l'ordre

    const trace = {
        x: data.map(row => row.key), // Utiliser la clé de la musique
        y: data.map(row => +row.streams), // Convertir les valeurs en nombres
        type: 'bar',
        marker: { color: '#1ed760' },
        hovertemplate: '%{x}',
        name: ''
    };

    const layout = {
        title: 'Clé des musiques les plus écouté',
        xaxis: {
            title: 'Clé des Musiques'
        },
        yaxis:{
            title:'Nombre d\'Écoutes'
         }
    };

    // Créer le graphique avec Plotly
    Plotly.newPlot('dataviz4', [trace], layout);
});
//dataviz 4