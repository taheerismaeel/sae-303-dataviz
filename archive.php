<?php get_header(); ?>

<main>
    <h1>Archive</h1>
</main>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

    <article>
        <h2><?php the_title(); ?></h2>
        <p>Par <?php the_author(); ?></p>
        <div><?php the_excerpt(); ?></div>
    </article>

<?php endwhile; else : ?>
    <p>Aucun article trouvé.</p>
<?php endif; ?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    <article>
        <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
        <div><?php the_excerpt(); ?></div>
    </article>
<?php endwhile; endif; ?>

<?php get_footer(); ?>
